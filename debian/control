Source: mplayer
Section: video
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 A Mennucc1 <mennucc1@debian.org>,
 Miguel A. Colón Vélez <debian.micove@gmail.com>,
 Reinhard Tartler <siretart@tauware.de>,
 Lorenzo Puliti <plorenzo@disroot.org>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/multimedia-team/mplayer.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/mplayer
Homepage: https://mplayerhq.hu
Build-Depends:
 debhelper-compat (= 13),
 dh-exec,
 ladspa-sdk,
 liba52-dev,
 libaa1-dev,
 libasound2-dev [linux-any],
 libass-dev,
 libaudio-dev,
 libavcodec-dev,
 libavformat-dev,
 libavutil-dev,
 libavutil-dev:native,
 libbluray-dev,
 libbs2b-dev,
 libcaca-dev,
 libcdio-cdda-dev,
 libcdio-paranoia-dev,
 libdca-dev,
 libdv-dev,
 libdvdnav-dev,
 libdvdread-dev,
 libenca-dev,
 libfaad-dev,
 libfontconfig-dev,
 libfreetype-dev,
 libfribidi-dev,
 libgif-dev,
 libgl-dev,
 libglvnd-dev,
 libgsm1-dev,
 libgtk2.0-dev,
 libjack-dev,
 libjpeg-dev,
 liblirc-dev,
 liblzo2-dev,
 libmad0-dev,
 libmng-dev,
 libmp3lame-dev,
 libmpeg2-4-dev,
 libmpg123-dev,
 libopenal-dev,
 libpostproc-dev,
 libpulse-dev,
 librtmp-dev,
 libsdl1.2-dev,
 libsmbclient-dev [!hurd-any],
 libspeex-dev,
 libswscale-dev,
 libtheora-dev,
 libtwolame-dev,
 libvdpau-dev,
 libvorbisidec-dev,
 libx11-dev,
 libx264-dev,
 libxext-dev,
 libxinerama-dev,
 libxss-dev,
 libxt-dev,
 libxv-dev,
 libxvidcore-dev,
 libxvmc-dev,
 libxxf86dga-dev,
 libxxf86vm-dev,
 pkgconf,
 x11proto-core-dev,
 nasm,
 zlib1g-dev
Build-Depends-Indep:
 docbook-xml,
 docbook-xsl,
 xsltproc
Rules-Requires-Root: no

Package: mplayer-gui
Architecture: any
Multi-Arch: foreign
Suggests:
 bzip2,
 fontconfig,
 fonts-freefont-ttf,
 mplayer-doc
Depends:
 mplayer,
 ${misc:Depends},
 ${shlibs:Depends}
Recommends: mplayer-skin
Description: movie player for Unix-like systems (GUI variant)
 MPlayer plays most MPEG, VOB, AVI, Ogg/OGM, VIVO,
 ASF/WMA/WMV, QT/MOV/MP4, FLI, RM, NuppelVideo, yuv4mpeg, FILM, RoQ, PVA files,
 supported by many native, XAnim, RealPlayer, and Win32 DLL codecs. It can
 also play VideoCD, SVCD, DVD, 3ivx, RealMedia, and DivX movies.
 .
 Another big feature of MPlayer is the wide range of supported output
 drivers. It works with X11, Xv, DGA, OpenGL, SVGAlib, fbdev,
 but also SDL.
 .
 This package includes the GUI variant of MPlayer.

Package: mencoder
Architecture: any
Multi-Arch: foreign
Suggests:
 bzip2,
 fontconfig,
 fonts-freefont-ttf,
 mplayer-doc
Depends:
 mplayer,
 ${misc:Depends},
 ${shlibs:Depends}
Description: MPlayer's Movie Encoder
 MPlayer plays most MPEG, VOB, AVI, Ogg/OGM, VIVO,
 ASF/WMA/WMV, QT/MOV/MP4, FLI, RM, NuppelVideo, yuv4mpeg, FILM, RoQ, PVA files,
 supported by many native, XAnim, RealPlayer, and Win32 DLL codecs. It can
 also play VideoCD, SVCD, DVD, 3ivx, RealMedia, and DivX movies.
 .
 This package contains mencoder, a simple movie encoder, designed to
 encode MPlayer-playable movies
 (AVI/ASF/OGG/DVD/VCD/VOB/MPG/MOV/VIV/FLI/RM/NUV/NET) to other
 MPlayer-playable formats. It can encode with various codecs, like DivX4
 (1 or 2 passes), libavcodec, PCM/MP3/VBRMP3 audio. Also has stream
 copying and video resizing capabilities.

Package: mplayer
Architecture: any
Multi-Arch: foreign
Suggests:
 bzip2,
 fontconfig,
 fonts-freefont-ttf,
 mplayer-doc,
 netselect | fping
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: movie player for Unix-like systems
 MPlayer plays most MPEG, VOB, AVI, Ogg/OGM, VIVO,
 ASF/WMA/WMV, QT/MOV/MP4, FLI, RM, NuppelVideo, yuv4mpeg, FILM, RoQ, PVA files,
 supported by many native, XAnim, RealPlayer, and Win32 DLL codecs. It can
 also play VideoCD, SVCD, DVD, 3ivx, RealMedia, and DivX movies.
 .
 Another big feature of MPlayer is the wide range of supported output
 drivers. It works with X11, Xv, DGA, OpenGL, SVGAlib, fbdev,
 but also SDL.
 .
 Not all of the upstream code is distributed in the source tarball.
 See the README.Debian and copyright files for details.

Package: mplayer-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Recommends:
 mplayer
Depends:
 ${misc:Depends}
Description: documentation for MPlayer
 This package contains the HTML documentation for MPlayer, a movie player for
 Unix-like systems. It is available in several languages.
 .
 You only need this package if you want to read the MPlayer manual. The manpages
 are included in the regular MPlayer package.
